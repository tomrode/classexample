class Line {
public:
	void setLength(double len);
	double getLength(void);
	Line();   // This is the constructor declaration
	~Line();  // This is the destructor: declaration

private:
	double length;
};