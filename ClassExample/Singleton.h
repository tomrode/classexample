#pragma once
#ifndef SINGLETON_H
#define SINGLETON_H

#include <iostream>

using namespace std;

class Singleton {

private:

	//static Singleton* instance;
	int data;

	// Private constructor so that no objects can be created.
	Singleton();
	
public:
	static Singleton* getInstance();
	
	int getData();
	
	void setData(int data);
	
};

#if 0
class Singleton
{
	static Singleton* s_instance;
	int data;

	// Private constructor so that no objects can be created
	Singleton()
	{
		data = 0;
	}

public:
	static Singleton* instance()
	{
		if (!s_instance)
			s_instance = new Singleton;
		return s_instance;
	}

	int get_value()
	{
		return this->data;
	}

	void set_value(int data)
	{
		this->data = data;
	}

};
  #endif
#endif

