// Main.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include<vector>
using namespace std;
#include "Time.h"
#include "Line.h"
#include "Math.h"
#include "Singleton.h"


// Types
typedef struct//VectorT
{
	int val;
	int val2;
}VECTOR_T;

struct subject {
	string name;
	int marks;
	int credits;
};

// Function Prototypes 
void RunLine(void);


// Namespaces 
namespace ns
{
	// Only declaring class here 
	class NameSpaceClass;
}

// Defining class outside 
class ns::NameSpaceClass
{
public:
	void display()
	{
		cout << "ns::NameSpaceClass::display()\n";
	}
};

int main()
{  
	int test;
	// Instantiate the objects
	Time t1(10, 50, 59);
	Time t2;
	Math m;

    std::cout << "Hello World!\n";
	cout << "\n";

	// singleton Example
	cout << "Singleton Example\n";
	Singleton* s = s->Singleton::getInstance();
	cout << s->getData() << endl;
	s->setData(100);
	cout << s->getData() << endl;
	cout << "\n";

	// Namespace Excersize
	cout << "Namespace Excersize\n";

	//Creating Object of geek Class 
	ns::NameSpaceClass obj;
	obj.display();
	cout << "\n";

	// Vector excersize
	cout << "Vector excersize\n";
	vector<int> example{ 1,2,3,4,5,6, 7 };
	vector<VECTOR_T> ComplexVec;
	vector<subject> sub;
	for (int x = 0; x != example.size(); ++x)
	{
		cout << example[x] << "- subscripting" << endl;
		cout << example.at(x) << "- calling at member" << endl;
	}

	// add elements 
	cout << "Adding elements to vector<int> example\n";
	for (int x = 7; x <= 10; ++x)
	{
		example.push_back(x);
		cout << example[x] << "- subscripting" << endl;
		cout << example.at(x) << "- calling at member" << endl;
	}
	//Push back new subject created with default constructor.
	sub.push_back(subject());

	//Vector now has 1 element @ index 0, so modify it.
	sub[0].name = "english";

	//Add a new element if you want another:
	sub.push_back(subject());

	//Modify its name and marks.
	sub[1].name = "math";
	sub[1].marks = 90;
	
	// adding elemnts to VECTOR_T
	for (int x = 0; x <= 10; ++x)
	{   
		//Push back new VECTOR_T created with default constructor.
		ComplexVec.push_back(VECTOR_T());

		ComplexVec[x].val = x;
		ComplexVec[x].val2 = (x+1);

		cout << ComplexVec[x].val << "ComplexVec.val" << endl;
		cout << ComplexVec[x].val2 << "ComplexVec.val2" << endl;

	}
	cout << "\n";

	// set the math numbers to use
	cout << "Running the math class\n";
    m.setNum(3,7);
	std::cout << "First number -> " << m.getNum1() << " Second number -> " <<  m.getNum2() <<  "\n";
	std::cout << "Factorial of number -> " << m.getNum1() << "\n";
	test = m.factNum();
	cout << "This is the factorial result -> " << test << "\n";

	cout << "Multiply two numbers together \n";
	test = m.mulTwoNum();
	cout << "This is the multiplication result ->" << test << "\n";
	cout << "\n";


	cout << "Running the Time class\n";
	t1.print();   // 10:50:59
	t2.print(); // 06:39:09
	t2.setTime(10, 50, 59);
	t2.print();  // 06:39:09

	if (t1.equals(t2))
		cout << "Two objects are equal\n";
	else
		cout << "Two objects are not equal\n";
	cout << "\n";


	cout << "Running the Line class\n";
	// Watch the object life cycle
	RunLine();
	
	std::cout << "The End of Main!\n";
	return 0;
}

void RunLine(void)
{
	// Instantiate Line object
	Line Runline;
	// set line length
	Runline.setLength(6.0);
	cout << "Length of line : " << Runline.getLength() << endl;
}



// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
